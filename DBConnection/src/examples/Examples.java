package examples;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;
import database.Queries;
import database.Query;
import database.vendors.MySQL;
import exceptions.InvalidDatasource;
import exceptions.NoConnectionException;


public class Examples {

	public static void main(String[] args) {
		Database mySQLDB = new MySQL("schema", "host", "port", "user", "password");
		try {
			ResultSet rs = mySQLDB.query(Queries.SAMPLE_QUERY);
			while(rs.next()){
				System.out.println(rs.getString(1));
			}
			rs.close();
			mySQLDB.getConnection().close();
		} catch (SQLException | NoConnectionException e) {
			e.printStackTrace();
		}
		
		queryAsThread();
	}
	
	//This way of executing a query enables you to create an extra thread for long time queries, 
	//so the application can do something else simultaneously
	public static void queryAsThread(){
		Database mySQLDB = new MySQL("schema", "host", "port", "user", "password");
		try {
			//Create a Query object with a defined SQL query and the Database object it should be executed on
			final Query query = new Query(Queries.SAMPLE_QUERY, mySQLDB);
			
			//You can choose whether exceptions should be displayed directly in a JOptionPane
			//or you handle them yourself. In our case we handle them in the following code
			query.setShowError(false);
			
			//Create a Runnable which will be executed after the query has returned its result
			Runnable handleQueryResult = new Runnable() {
				@Override
				public void run() {
					try {
						//HERE COMES THE CODE WHICH SHOULD BE EXECUTED, AFTER THE QUERY RETURNED ITS ENTIRE RESULT
						/*
						ResultSet rs = query.getResultSet();
						while(rs.next()){
							System.out.println(rs.getString(1));
						}
						rs.close();
						query.getDatabase().getConnection().close();
						*/
						
						//FAKE LONG TERM ACTION
						Thread.sleep(5000);
					} catch (Exception e) {
						// EXCEPTION IS THROWN IF QUERY COULDNT BE EXECUTED AND THEREFORE NO RESULTSET WAS CREATED
						e.printStackTrace();
					}
				}
			};
			
			query.setAfterWork(handleQueryResult);
			
			//tells the query to execute the AfterWork Thread even if an error occurred while processing the query
			query.setAlwaysExecuteAfterWork(true);
			
			query.start();
			
			Thread progressListener = new Thread(new Runnable() {
				@Override
				public void run() {
					while(query.isAlive()){
						System.out.println("Waiting for query to finish...");
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							System.out.println("Listener was interrupted!");
							break;
						}
					}
					System.out.println("Query finished or listener was interrupted!");
				}
			});
			
			progressListener.start();
			
			
		} catch (IllegalArgumentException | InvalidDatasource e){
			//THIS EXCEPTION IS THROWN IF THE SQL QUERY OR DATABASE OBJECT ARE NULL AND THEREFORE NO QUERY OBJECT COULD BE CREATED
			e.printStackTrace();
		}
	}

}
