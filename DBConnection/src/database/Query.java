package database;

import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;

import exceptions.InvalidDatasource;

public class Query extends Thread {

	private String query = "";
	private Database database = null;
	private Statement statement = null;
	private Exception queryException = null;
	private boolean showError = true;
	private Runnable afterWork = new Runnable() {
		@Override
		public void run() {
		}
	};
	private boolean afterWorkRunning = false;
	private boolean alwaysExecuteAfterWork = false;

	public Query(String query, Database db) throws IllegalArgumentException,
			InvalidDatasource {
		if (query == null || query.isEmpty()) {
			throw new IllegalArgumentException("Query = null or empty!");
		}
		if (db == null) {
			throw new InvalidDatasource("Database object = null!");
		}
		setQuery(query);
		setDatabase(db);
		setQueryException(getDatabase().getConnectionException());
	}

	public Query(String query, Database db, boolean showError)
			throws IllegalArgumentException, InvalidDatasource {
		this(query, db);
		setShowError(showError);
	}

	public Query(String query, Database db, Runnable afterWork)
			throws IllegalArgumentException, InvalidDatasource {
		this(query, db);
		setAfterWork(afterWork);
	}

	public Query(String query, Database db, Runnable afterWork,
			boolean showError) throws IllegalArgumentException,
			InvalidDatasource {
		this(query, db);
		setAfterWork(afterWork);
		setShowError(showError);
	}

	@Override
	public void run() {
		try {
			setStatement(getDatabase().getConnection().createStatement());
			System.out.println("Execute query (on " + getDatabase().toString()
					+ "):\n" + query);
			getStatement().executeQuery(getQuery());
			
		} catch (Exception e) {
			e.printStackTrace();
			setQueryException(e);
			if (isShowError()) {
				JOptionPane.showMessageDialog(null, 
						"An error ocurred while executing following query (on "
								+ getDatabase().toString() + "):\n"
								+ getQuery()
								+ "\n"
								+ e);
			}
		}
		if((getQueryException() != null && alwaysExecuteAfterWork ) || getQueryException() == null){
			setAfterWorkRunning(true);
			getAfterWork().run();
		}
		
	}

	public ResultSet getResultSet() throws Exception {
		if (statement != null) {
			return statement.getResultSet();
		}
		throw getQueryException();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public Statement getStatement() throws Exception {
		if (statement != null) {
			return statement;
		}
		throw getQueryException();
	}

	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	public Exception getQueryException() {
		return queryException;
	}

	public void setQueryException(Exception queryException) {
		this.queryException = queryException;
	}

	public boolean isShowError() {
		return showError;
	}

	public void setShowError(boolean showError) {
		this.showError = showError;
	}

	public Runnable getAfterWork() {
		return afterWork;
	}

	public void setAfterWork(Runnable afterWork) {
		this.afterWork = afterWork;
	}

	public boolean isAfterWorkRunning() {
		return afterWorkRunning;
	}

	private void setAfterWorkRunning(boolean afterWorkRunning) {
		this.afterWorkRunning = afterWorkRunning;
	}

	public boolean isAlwaysExecuteAfterWork() {
		return alwaysExecuteAfterWork;
	}

	public void setAlwaysExecuteAfterWork(boolean alwaysExecuteAfterWork) {
		this.alwaysExecuteAfterWork = alwaysExecuteAfterWork;
	}

	public void cancel() throws Exception {
		System.out.println("Cancel query...");
		getStatement().cancel();
		System.out.println("Canceled query!");
	}

	@Override
	public void interrupt() {
		System.out.println("interrupting query object...");
		try {
			if (!isAfterWorkRunning()) {
				// throw new Exception("Can not cancel query!");
				cancel();
				// don't show interrupt exception to user as
				// it is not a real error
				setShowError(false);
			}
			super.interrupt();
		} catch (Exception e) {
			e.printStackTrace();
			if (isShowError()) {
				JOptionPane.showMessageDialog(null, 
						"An error ocurred while executing following query (on "
								+ getDatabase().toString() + "):\n"
								+ getQuery()
								+ "\n"
								+ e);
			}
		}
	}
}
