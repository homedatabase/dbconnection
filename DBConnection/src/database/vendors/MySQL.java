package database.vendors;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import database.Database;

/**
 * This class holds vendor specific driver and connection informations
 * @author Eric W�nsche
 *
 */
public class MySQL extends Database{
	
	/**
	 * Instantiates a new {@link Database} object and tries to establish a connection to it.
	 * @param schema - the database schema
	 * @param host - host address of the DB system
	 * @param port - port on which the DB system is running
	 * @param user - DB user
	 * @param pass - DB user-password
	 * @throws ClassNotFoundException if the connection driver could not be loaded
	 * @throws SQLException if a database access error occurs
	 */
	public MySQL(String schema, String host, String port,
			String user, String pass) {
		super(schema, host, port, user, pass);
	}
	
	/**
	 * Puts all connection information together into a connection URL and tries to connect to the DBMS
	 * @throws ClassNotFoundException if the connection driver could not be loaded
	 * @throws SQLException if a database access error occurs
	 */
	protected void establishConnection() {
		//load driver class
		System.out.println("Establishing connection to MySQL DBMS...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//initiate connection
			Properties connectionProperties = new Properties();
			connectionProperties.put("user", getUser());
			connectionProperties.put("password", getPass());
			connectionProperties.put("databaseName", getSchemaName());
			setConnectionURL("jdbc:mysql://"+getHost()+":"+getPort()+"/"+getSchemaName());
			System.out.println("Connect: "+getConnectionURL()+" with user "+getUser());
			setConnection(DriverManager.getConnection(getConnectionURL(), connectionProperties));
			System.out.println("Connection has been established.");
			setConnectionException(null);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			setConnectionException(e);
			System.out.println("Connection has NOT been established.");
		}
	}

	@Override
	public String getDBVendor() {
		return "MySQL";
	}
}