package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exceptions.NoConnectionException;

/**
 * Provides access and query functionality for Databases.
 * This class is used as a super class for all different vendors.
 * The class works with a singleton instance, so that no 
 * further uncontrolled connections can be made.
 * 
 * @author Eric W�nsche
 *
 */
public abstract class Database {
	
	public static final String VENDOR_MYSQL = "MySQL";
	public static final String VENDOR_ORACLE = "Oracle";
	
	/**
	 * the database schema that should be used
	 */
	private String schema = "";
	
	/**
	 * Address of the DBMS
	 */
	private String host = "";
	
	/**
	 * Port on which the DBMS is located
	 */
	private String port = "";
	
	/**
	 * The DBMS User that should be used
	 */
	private String user = "";
	
	/**
	 * The password for the DBMS User
	 */
	private String pass = "";
	
	/**
	 * Vendor (DBMS) specific connection URL
	 */
	private String connectionURL = "jdbc:oracle:thin:@"+host+":"+port+":"+schema;
	
	/**
	 * Connection element upon which queries are sent and proceeded
	 */
	private Connection connection = null;
	
	private Exception connectionException = new NoConnectionException();
	
	/**
	 * Returns the name of the currently set database vendor (system)
	 */
	public abstract String getDBVendor();
	
	/**
	 * @return The currently configured schema/database name that is used
	 */
	public String getSchemaName() {
		return schema;
	}

	/**
	 * set the schema/database name that should be used
	 * @param dbName - schema/database name
	 */
	public void setSchemaName(String dbName) {
		this.schema = dbName;
	}

	/**
	 * @return The currently configured host that is used
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Set the address / host of the DBMS
	 * @param host - address / host of the DBMS
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return The port on which the DBMS is running
	 */
	public String getPort() {
		return port;
	}

	/**
	 * Set the port on which the DBMS is running
	 * @param port the port on which the DBMS is running
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the user to use with the DBMS
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user user to use with the DBMS
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the currently set password for the DBMS user
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * Set the password to be used with the current DBMS user
	 * @param pass the password for the DBMS user
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * @return the vendor specific connection URL
	 */
	public String getConnectionURL() {
		return connectionURL;
	}

	/**
	 * set the vendor specific connection URL to the database
	 * @param connectionURL vendor specific connection URL to the database
	 */
	public void setConnectionURL(String connectionURL) {
		this.connectionURL = connectionURL;
	}

	/**
	 * @return the {@link Connection} element of the DBMS or NULL if no connection has been established
	 * @throws NoConnectionException 
	 */
	public Connection getConnection() throws NoConnectionException {
		if(connection == null){
			establishConnection();
		}
		if(connection != null){
			return connection;
		}
		throw new NoConnectionException(getConnectionException());
	}

	/**
	 * @param connection the {@link Connection} element of the DBMS or NULL if no connection has been established
	 */
	protected void setConnection(Connection connection) {
		this.connection = connection;
	}


	public Exception getConnectionException() {
		return connectionException;
	}

	public void setConnectionException(Exception connectionException) {
		this.connectionException = connectionException;
	}
	
	public Database(String schema, String host, String port,
			String user, String pass) {
		setSchemaName(schema);
		setHost(host);
		setPort(port);
		setUser(user);
		setPass(pass);
		//establishConnection();
	}
	
	protected abstract void establishConnection();

	/**
	 * Sends a query string to the DBMS and returns the OPEN {@link ResultSet} of it. 
	 * Make sure to close the {@link ResultSet} after using its information.
	 * @param query The query as a String
	 * @return the OPEN {@link ResultSet} of the query
	 * @throws SQLException if a database access error occurs
	 * @throws NoConnectionException 
	 */
	public ResultSet query(String query) throws SQLException, NoConnectionException{
		Statement stmt = getConnection().createStatement();
		System.out.println("Execute query (on " + toString() + "):\n" + query);
	    return stmt.executeQuery(query);
	}
	
	/**
	 * Sends a query string to the DBMS and returns the OPEN {@link Statement} of it. 
	 * Make sure to close the {@link Statement} after using its information.
	 * @param query The query as a String
	 * @return the OPEN {@link Statement} of the query
	 * @throws SQLException if a database access error occurs
	 * @throws NoConnectionException 
	 */
	public Statement queryStatement(String query) throws SQLException, NoConnectionException{
		Statement stmt = getConnection().createStatement();
		System.out.println("Execute query (on " + toString() + "):\n" + query);
	    stmt.executeQuery(query);
	    return stmt;
	}
	
	@Override
	public String toString() {
		return getHost()+":"+getPort()+":"+getSchemaName();
	}
}
