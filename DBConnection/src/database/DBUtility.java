package database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

/**
 * This class holds some useful methods
 * @author Eric W�nsche
 *
 */
public class DBUtility {

	/**
	 * Transforms an OPEN {@link ResultSet} into a {@link DefaultTableModel}
	 * @param rs - the OPEN {@link ResultSet} of a query
	 * @return a {@link DefaultTableModel}
	 * @throws SQLException if a database access error occurs or this method is called on a closed result set
	 */
	public static DefaultTableModel getTableModel(ResultSet rs) throws SQLException{
		//initialize variables
		ResultSetMetaData rsmd = rs.getMetaData();
		int colCount = rsmd.getColumnCount();
		//System.out.println("Cols: "+colCount);
		String[] columnNames = new String[colCount];
		String[][] data;
		ArrayList<String[]> dataList = new ArrayList<String[]>();
		
		//get column names
		for(int c = 1; c <= colCount; c++){
			columnNames[c-1] = rsmd.getColumnLabel(c);
		}
		
		//get values
		while(rs.next()){
			String[] values = new String[colCount];
			for(int c = 1; c <= colCount; c++){
				values[c-1] = rs.getString(c);
			}
			dataList.add(values);
		}
		rs.getStatement().close();
		
		//transform values
		data = new String[dataList.size()][];
		for(int i = 0; i < dataList.size(); i++){
			data[i] = dataList.get(i);
		}
		return new DefaultTableModel(data, columnNames){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}
	
	public static ArrayList<Object[]> getDataList(ResultSet rs) throws SQLException{
		//initialize variables
		ArrayList<Object[]> dataList = new ArrayList<Object[]>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int colCount = rsmd.getColumnCount();
		
		//get values
		while(rs.next()){
			Object[] values = new Object[colCount];
			for(int c = 1; c <= colCount; c++){
				values[c-1] = rs.getObject(c);
			}
			dataList.add(values);
		}
		rs.getStatement().close();
		return dataList;
	}
}
