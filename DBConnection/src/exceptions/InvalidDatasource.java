package exceptions;

import database.Database;

/**
 * Exception class representing the error, that a {@link Database} object 
 * is missing or not functional
 * 
 * @author Eric W�nsche
 *
 */
public class InvalidDatasource extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidDatasource(){
		super();
	}

	public InvalidDatasource(String message){
		super(message);
	}
	
	public InvalidDatasource(Throwable cause){
		super(cause);
	}
	
	public InvalidDatasource(String message, Throwable cause){
		super(message, cause);
	}
}
