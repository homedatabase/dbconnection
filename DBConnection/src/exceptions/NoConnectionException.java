package exceptions;

import database.Database;

/**
 * Exception class to represent the error that there is no connection 
 * established to a {@link Database} object. The default message of the 
 * exception is {@value #defaultMessage} 
 * 
 * @author Eric W�nsche
 *
 */
public class NoConnectionException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String defaultMessage = "There is no connection established to the database!";
	
	public NoConnectionException(){
		super(defaultMessage);
	}
	
	public NoConnectionException(Throwable cause){
		super(defaultMessage, cause);
	}

	public NoConnectionException(String message, Throwable cause){
		super(message, cause);
	}
}
